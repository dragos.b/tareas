export default class Task {
    constructor(id, title, desc, completed) {
        this.id         = id
        this.title      = title
        this.desc       = desc
        this.completed  = completed
    }
}