export { addTask, existsTasks }
import * as locals from './localStorage.js'
import Task from './task.js'

const table = document.getElementById("table")
let task_id = 1
let total_tasks = []

function existsTasks() {
    locals.readLocalStorage(total_tasks)
    if (total_tasks.length) {
        task_id = total_tasks[total_tasks.length - 1].id + 1

        for (let i = 0; i < total_tasks.length; i++)
            drawTask(
                total_tasks[i].id,
                total_tasks[i].title,
                total_tasks[i].desc,
                total_tasks[i].completed)
    }
}

function addTask() {

    const name = document.getElementById("task_name")
    const description = document.getElementById("task_description")

    if (!isEmpty(name.value, description.value)) {
        var tit  = name.value.toLowerCase()
        var desc = description.value.toLowerCase()
        
        tit  = firstCharToUpper(tit)
        desc = firstCharToUpper(desc)

        total_tasks.push(new Task(task_id, tit, desc, false))
        locals.writeLocalStorage(total_tasks)
        drawTask(task_id, tit, desc, false)

        task_id++
        name.value = ""
        description.value = ""
    }
}

function firstCharToUpper (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

function isEmpty(name, description) {
    return !name.length || !description.length ? true : false
}


function saveChecked(id) {
    for(var i=0; i<total_tasks.length; i++) {
        if (id == total_tasks[i].id) {
            total_tasks[i].completed = !total_tasks[i].completed
            break 
        }
    }
    locals.writeLocalStorage(total_tasks)
}

function removeTask(id) {
    if (document.getElementById(`check_${id}`).checked) {
        document.getElementById(`row_${id}`).remove()

        for (let i = 0; i < total_tasks.length; i++) {
            if (total_tasks[i].id == id) {
                total_tasks.splice(i, 1)
                i = total_tasks.length
            }
        }
        locals.writeLocalStorage(total_tasks)
    }
}

function editTask(id) {
    let modal = new bootstrap.Modal(document.getElementById("editModal"), null)
    modal.show()
}

function drawTask(id, name, description, completed) {

    let tdTitle, tdDescription, checkbox, editButton, deleteButton

    tdTitle       = createTd(name)
    tdDescription = createTd(description)
    deleteButton  = createDelButton(id)
    editButton    = createEditButton(id)
    checkbox      = createCheckbox(id, completed)

    var tableElements = [
        tdTitle, 
        tdDescription, 
        checkbox, 
        editButton, 
        deleteButton]

    var row = table.insertRow()
    row.id = `row_${id}`

    for (var i = 0; i < tableElements.length; i++) {
        row.insertCell()
        row.children[i].appendChild(tableElements[i])
    }
}

function createTd(text) {
    var td = document.createElement("td")
    td.innerHTML = text
    return td
}

function createDelButton(id) {
    var btn = document.createElement("button")
    btn.classList.add("btn", "btn-danger")
    btn.innerHTML = `<i class="fa fa-trash"></i>`
    btn.id = `${id}`
    btn.onclick = function () {
        removeTask(btn.id)
    }
    return btn
}

function createEditButton(id) {
    var btn = document.createElement("button")
    btn.classList.add("btn", "btn-primary")
    btn.innerHTML = `<i class="fa fa-pencil"></i>`
    btn.id = `${id}`
    btn.onclick = function () {
        editTask(btn.id)
    }
    return btn
}


function createCheckbox(id, completed) {
    var chkbx = document.createElement("input")
    chkbx.type = "checkbox"
    chkbx.classList.add("form-check-input")
    chkbx.id = `check_${id}`
    chkbx.checked = completed

    chkbx.onclick = function () {
        saveChecked(id)  
    }
    return chkbx
}